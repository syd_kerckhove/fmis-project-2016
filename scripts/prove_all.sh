source scripts/lib.sh

check_single () {
  check "Proving lemma's: ${1}" tamarin-prover "${1}" --prove --quit-on-warning
}

check_all () {
  for i in $(find theory -type f -name "*.spthy")
  do
    check_single "${i}"
  done
}

check_all
