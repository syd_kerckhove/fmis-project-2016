#!/bin/bash

# Abort on error
set -e

./scripts/check_consistency.sh
./scripts/prove_all.sh
